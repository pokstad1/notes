---
title: Multi-Module Aware Repository Versioning
tags: [go, go-modules, semantic-versioning]
---

Go supports multi-module aware repos, meaning a repository that might hold more
than one Go module. I've contributed to one such repository in gitaly-proto:

- [gitlab.com/gitlab-org/gitaly-proto/go/gitalypb][gitalypb]
- [gitlab.com/gitlab-org/gitaly-proto/go/internal][internal]

This allows our tooling in CI to version dependencies independent of the code we
are generating for gRPC and protobuf.

What's really interesting about this topic is how we can
version a ["submodule"][using submodules] independently of another submodule in the same repo.
Normally, a git tag applies a version tag to the entire repo, meaning that many
submodules are versioning in lockstep.

However, Go has a convention for tagging such that a tag only applies to a
specific module. In the future, we may find the need to tag only the gitalypb,
in which case we could assign a v2.0.0 tag to it like so:

`go/gitalypb/v2.0.0`

See [What are Multi-Module Repositories][multi-module repos] for more information.

[gitalypb]: https://gitlab.com/gitlab-org/gitaly-proto/blob/master/go/gitalypb/go.mod "gitalypb module"
[internal]: https://gitlab.com/gitlab-org/gitaly-proto/blob/master/go/internal/go.mod "internal module"
[multi-module repos]: https://github.com/golang/go/wiki/Modules#what-are-multi-module-repositories
[using submodules]: https://github.com/go-modules-by-example/index/blob/master/009_submodules/README.md