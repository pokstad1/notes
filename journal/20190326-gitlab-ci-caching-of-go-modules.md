---
title: GitLab CI caching of Go modules
tags: [go, go-modules, gitlab-ci]
---
Enabling the caching of downloaded Go modules between CI pipelines can be done
quite trivially using the GitLab cache feature. The first observed solution to
this problem was seen in [gitlabktl's CI configuration][gitlabktl-ci-cfg]. This
can be further simplified to the following snippet:

```yaml
image: golang:1.12

cache:
  paths:
    - .go/pkg/mod

variables:
  GOPATH: $CI_PROJECT_DIR/.go

before_script:
  - mkdir -p .go
```

Some important things to note:

- GitLab CI cache does not work outside the project workspace. This is why the
  `GOPATH` is moved to a hidden directory inside the project workspace:
  `GOPATH: $CI_PROJECT_DIR/.go`
- Cache is a best effort optimization. It can sometimes fail and will then
  require fetching dependencies over the network.

[gitlabktl-ci-cfg]: https://gitlab.com/gitlab-org/gitlabktl/blob/8df90a8483b9be5d7636599fb9e319166f7a33b9/.gitlab-ci.yml