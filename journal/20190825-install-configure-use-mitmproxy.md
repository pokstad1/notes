---
title: mitmproxy
tags: [networking, mitm, proxying, macos]
---

1. `brew install mitmproxy`
1. `mitmproxy --mode socks5 --showhost`
1. Enable system SOCKS proxy for current network interface:
   1. Open `System Preferences` -> `Networking` -> `Advanced` -> `Proxies`
   1. Enable `SOCKS Proxy`. Set proxy server address to `127.0.0.1` and port to `8080`.
   1. Click `OK`
   1. Click `Apply`
1. Install proxy certs
   1. In browser, open `mitm.it`
   1. Click Apple logo to download cert
   1. Double click downloaded file to install
   1. Enable certs for system wide SSL
      1. Open `Keychain` app
      1. Double click `mitmproxy` cert
      1. Expand `Trust` section
      1. For `Secure Sockets Layer (SSL)` select `Always Trust`
      1. Close window
      1. Enter password to apply change

References:

- https://blogs.msdn.microsoft.com/aaddevsup/2018/04/11/tracing-all-network-machine-traffic-using-mitmproxy-for-mac-osx/