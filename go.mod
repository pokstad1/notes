module gitlab.com/pokstad1/notes

go 1.12

require (
	github.com/golang/protobuf v1.3.5
	google.golang.org/grpc v1.28.1
)
