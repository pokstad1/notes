package snippets_test

import (
	"io/ioutil"
	"os/exec"
	"testing"
)

// TestGoGen shows how a test fixture can be generated line-by-line via go-
// generate statements.
//go:generate mkdir -p testdata/gogen
//go:generate sh -c "echo 'test me bro' >testdata/gogen/sample.txt"
//go:generate sh -c "echo 'do you even test, bro?' >>testdata/gogen/sample.txt"
func TestGoGen(t *testing.T) {
	err := exec.Command("go", "generate").Run()
	if err != nil {
		t.Fatal(err)
	}

	out, err := ioutil.ReadFile("testdata/gogen/sample.txt")
	if err != nil {
		t.Fatal(err)
	}

	if string(out) != "test me bro\ndo you even test, bro?\n" {
		t.Fatalf("did not get expected file: %s", string(out))
	}
}
