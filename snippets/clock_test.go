package snippets

import (
	"testing"
	"time"
)

var clock = time.Now

func TestMockClock(t *testing.T) {
	defer func(old func() time.Time) { clock = old }(clock)

	timeQ := make(chan time.Time)
	clock = func() time.Time { return <-timeQ }

	done := make(chan struct{})
	go func() {
		defer close(done)
		t.Logf("The current time is %s", clock().UTC())
	}()

	// now calls to clock are controlled precisely by injecting into
	// the timeQ:
	timeQ <- time.Unix(1, 0)
	<-done
}
