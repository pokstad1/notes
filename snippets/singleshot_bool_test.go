package snippets_test

import (
	"sync"
	"testing"
)

func TestSingleShotBool(t *testing.T) {
	t.Skip("skipped because race condition exists: " +
		"https://gitlab.com/pokstad1/notes/-/jobs/202638484",
	)
	singleShot := false
	wait := make(chan struct{})
	wg := sync.WaitGroup{}

	for i := 0; i < 2; i++ {
		wg.Add(1)
		go func() {
			defer wg.Done()

			<-wait
			singleShot = true
			t.Logf("singleShot: %t", singleShot)
		}()
	}

	close(wait) // start the race!

	wg.Wait() // wait for all racers

	t.Logf("after all the chaos, singleShot is %t", singleShot)

}
