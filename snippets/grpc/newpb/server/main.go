package main

import (
	"flag"
	"fmt"
	"log"
	"net"
	"os"
	"os/signal"

	"gitlab.com/pokstad1/notes/snippets/grpc/newpb"
	"google.golang.org/grpc"
)

var port = flag.Int("port", 9999, "port to listen on")

func main() {
	flag.Parse()

	gs := grpc.NewServer()
	defer gs.Stop()

	newpb.RegisterTestServiceServer(gs, server{})

	lis, err := net.Listen("tcp", fmt.Sprintf(":%d", *port))
	if err != nil {
		log.Fatal(err)
	}
	log.Print(lis)

	go func() { log.Print(gs.Serve(lis)) }()

	exitSig := make(chan os.Signal)
	signal.Notify(exitSig, os.Interrupt)
	<-exitSig
	log.Print("Exiting...")
}

type server struct{}

func (s server) TestMethod(stream newpb.TestService_TestMethodServer) error {
	req, err := stream.Recv()
	if err != nil {
		return err
	}

	log.Printf("request: %#v", req)
	resp := &newpb.TestResponse{Bar: req.Foo + " woof"}
	log.Printf("server sending response: %#v", resp)

	return stream.Send(resp)
}
