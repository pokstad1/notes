package grpc_test

import (
	"context"
	"io"
	"os"
	"os/exec"
	"testing"
	"time"

	"gitlab.com/pokstad1/notes/snippets/grpc/oldpb"
	"google.golang.org/grpc"
)

//go:generate protoc --go_out=paths=source_relative,plugins=grpc:./oldpb -I./oldpb ./oldpb/service.proto
//go:generate protoc --go_out=paths=source_relative,plugins=grpc:./newpb -I./newpb ./newpb/service.proto
func TestCompatibility(t *testing.T) {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	cmd := exec.CommandContext(ctx, "go", "run", "./newpb/server", "-port", "666")
	cmd.Stderr = os.Stderr

	if err := cmd.Start(); err != nil {
		t.Fatal(err)
	}
	defer cmd.Process.Kill()

	cc, err := grpc.DialContext(ctx, "127.0.0.1:666",
		grpc.WithBlock(),
		grpc.WithInsecure(),
	)
	if err != nil {
		t.Fatal(err)
	}

	oldCC := oldpb.NewTestServiceClient(cc)
	stream, err := oldCC.TestMethod(ctx)
	if err != nil {
		t.Fatal(err)
	}

	if err := stream.Send(&oldpb.TestRequest{Foo: "meow"}); err != nil {
		t.Fatal(err)
	}

	resp, err := stream.CloseAndRecv()
	if err != nil && err != io.EOF {
		t.Fatal(err)
	}
	t.Logf("%#v", resp)
}
