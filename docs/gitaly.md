# Gitaly Notes

## How-To

- Tag a Gitaly release in Slack: `/chatops run gitaly tag vX.Y.Z-rcW`
- Update Gitaly CI build and test images:
  - Create an MR in https://gitlab.com/gitlab-org/gitlab-build-images to edit the `.gitlab-ci.yml` file to add the relevant containers.
  - Containers are built according to a naming convention.
  - Git and Go and other software built from source can be added/removed/updated in `scripts/custom-docker-build`
  - Example: https://gitlab.com/gitlab-org/gitlab-build-images/-/merge_requests/298

## TIL

- 2020-05-19
  - https://gitlab.com/gitlab-org/gitlab/-/issues/216619#note_345415655
    - Git pushes from Gitaly do not invoke a pre-push hook.
    - The remote service RPC `/gitaly.RemoteService/UpdateRemoteMirror` is a
      rare Ruby-based RPC that uses the Git CLI instead of libgit2.
    - Environment variables provided to Git allow the remote origin to be
      trusted based on a known public key (e.g. GitHub's public key).
- 2020-06-04
  - - https://gitlab.com/gitlab-org/gitaly/-/blob/master/ruby/lib/gitlab/git/repository.rb#L913
      - Worktrees are removed at the end of rebases and squashes