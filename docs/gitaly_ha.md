# Gitaly High Availability (HA)
Gitaly HA is an active-active cluster configuration for resilient git operations. [Refer to our specific requirements](https://gitlab.com/gitlab-org/gitaly/issues/1332).

## Terminology
The following terminology may be used within the context of the Gitaly HA project:

- Shard - partition of the storage for all repos. Each shard will require redundancy in the form of a qurom of nodes (at least 3 when optimal) to maintain HA.
- Reverse Proxy (*TODO: need better name to avoid confusion*) - transparent front end to all Gitaly shards. The reverse proxy ensures that all gRPC calls are forwarded to the correct shard by consulting the coordinator. The reverse proxy also ensures that write actions are performed transactionally. 
- Node - performs the actual git read/write operations to/from disk. Has no knowledge of other nodes/shards/proxies/coordinators as the Gitaly service existed prior to HA.

## Design
The high level design takes a reverse proxy approach to fanning out write requests to the appropriate nodes

```
               ┌──────────────────────────────────────────────┐
               │                  Gitaly HA                   │
               │             High Level Overview              │
               └──────────────────────────────────────────────┘
                                               ┌───────────────────────┐
                    ┌─────────┐                Shards    ┌──────────┐  │─┐
                  ┌─┤Reverse  │                │         │  Gitaly  │  │ │─┐
                ┌─┤ │Proxies  │            ┌───┼────────▶│   Node   │  │ │ │
                │ │ │         │            │   │         └──────────┘  │ │ │
                │ │ │         │            │   │         ┌──────────┐  │ │ │
                │ │ │         │            │   │         │  Gitaly  │  │ │ │
────────────────┼─┼▶│         │────────────┼───┼────────▶│   Node   │  │ │ │
   Gitaly gRPC  │ │ │         │  Fans out  │   │         └──────────┘  │ │ │
     request    │ │ │         │   write    │   │         ┌──────────┐  │ │ │
                │ │ │         │  requests  │   │         │  Gitaly  │  │ │ │
                │ │ │         │            └───┼────────▶│   Node   │  │ │ │
                │ │ └───────┬─┘                │         └──────────┘  │ │ │
                │ └──────┬┬─┘                  └───────────────────────┘ │ │
                └────────┼┘                      └───────────────────────┘ │
                         │                         └───────────────────────┘
                         │
                         │Discover
                         │ shard
                         │for repo
                         │
                         │
                         ▼
            ┌─────────────────────────┐
            │Coordinator -            │
            │Wrapper around existing  │
            │datastore                │
            │(e.g. Postgres or Redis) │
            └─────────────────────────┘
```

*Note: above diagram can be rerendered by editing the [Monodraw file gitaly_ha.monopic](gitaly_ha.monopic)*

## Phases
1. Simple proxy that passes all traffic to the indicated

## Notes
* Existing discussions
	* Requirements: https://gitlab.com/gitlab-org/gitaly/issues/1332
	* Design: https://gitlab.com/gitlab-org/gitaly/issues/1335
* Prior art
	* Stemma by Palantir
		* [Announcement](https://medium.com/palantir/stemma-distributed-git-server-70afbca0fc29)
		* Extends jgit (java git implementation)
	* Spokes by Github
		* Application layer approach: uses underlying git software to propagate changes to other locations.
	* Bitbucket Data Center (BDC)
		* [BDC FAQ](https://confluence.atlassian.com/enterprise/bitbucket-data-center-faq-776663707.html)
	* Ketch by Google (no longer maintained)
		* [Sid's comment on performance issue](https://news.ycombinator.com/item?id=13934698)
		* Also jgit based
* gRPC proxy considerations
	* [gRPC Proxy library](https://github.com/mwitkow/grpc-proxy)
		* Pros
			* Handles all gRPC requests generically
		* Cons
			* Lack of support
				* [See current importers of project](https://godoc.org/github.com/mwitkow/grpc-proxy/proxy?importers)
			* Low level implementation requires knowledge of gRPC internals
	* Custom code generation
		* Pros
			* Simple and maintainable
			* Allows us to handwrite proxy code and later automate with lessons learned via code generation
		* Cons
			* Process heavy; requires custom tooling
			* Requires a way to tell which methods are read/write
				* [See WIP for marking modifying RPCs](https://gitlab.com/gitlab-org/gitaly-proto/merge_requests/228)
	* See also:
		* [nRPC](https://github.com/nats-rpc/nrpc) - gRPC via NATS
		* [grpclb](https://github.com/bsm/grpclb) - gRPC load balancer
* Complications
	* Existing Rails app indicates the Gitaly instance that a request is destined for (e.g. request to modify repo X should be directed to gitaly #1).
		* This means that rails app must be kept in the loop about any changes made to the location of a repo.
		* This may be mitigated by changing the proxy implementation to intepret the destination address as a reference to a shard rather than a specific host. This might open the door to allowing for something like consistent hashing.
