# Go Programing Language

# Tips

- When creating a cleanup function for restoring global state, you can follow 
  the old-current-defer pattern:
    ```go
    defer func(old t) { current = old} (current)
    ```
- Code that relies on time limits expiring is hard to test deterministically. By
  using package vars, you can enable common time functions to be overriden with
  deterministic versions. See [snippets/clock_test.go](../snippets/clock_test.go).

## Articles
- [Using C++ Libraries in Go](https://medium.com/mysterium-network/golang-c-interoperability-caf0ba9f7bf3)

## Interesting Videos
- FOSDEM 2019
    - [Go Lighting Talks](https://video.fosdem.org/2019/UD2.120/)
        - 14:29 - Go performance tuning
            - How Uber tracked a performance regression using a forked Go compiler to insert print messages when the stack size was increased

## Reusable Comments
- Why use value receivers over pointers
    - https://gitlab.com/gitlab-org/gitaly/merge_requests/1068#note_143751065
