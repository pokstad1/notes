# Todos
Things I need to familiarize myself with to succeed. Add new incomplete items to the top of lists so that old completed items fall to the bottom.

* [ ] Ruby - find a good book for modern and idiomatic Ruby
	- [Eloquent Ruby](https://www.amazon.com/Eloquent-Ruby-Addison-Wesley-Professional/dp/0321584104)
	- [Well Grounded Rubyist](https://www.amazon.com/Well-Grounded-Rubyist-David-Black/dp/1617291692)
	- [Programming Ruby](https://www.amazon.com/Programming-Ruby-1-9-2-0-Programmers/dp/1937785491/)
* [ ] Gitlab Development Kit
* [ ] LabKit - standardized Go framework for Gitlab service development 
* [ ] Radix trees
	- [HN: Radix trees being used to block IP's](https://news.ycombinator.com/item?id=18921058)
		- [How Radix trees are using in Hashicorp](https://speakerdeck.com/armon/radix-trees-transactions-and-memdb)
		- [Hashicorp implementation with immutability and lock free reads](https://github.com/hashicorp/go-immutable-radix)
* [ ] Merkle trees
