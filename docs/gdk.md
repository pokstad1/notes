---
tags: [gdk, gitaly]
---
# GDK Notes
- Gitaly
  - To update GDK with a working copy of Gitaly:
    - Disable auto-update so that your changes are not replaced with GDK pinned version:
      ```yaml
      # gdk.yml
      gitaly:
        auto_update: false
      ```
    - Tell GDK to rebuild Gitaly/Praefect: `make gitaly-setup`
  
- Praefect
  - Disable Praefect:
    ```yaml
    # gdk.yml
    praefect:
      enabled: false
    ```

- Enable SSHD  
  - Uncomment SSHD in the GDK Procfile:
    ```Procfile
    sshd: exec /usr/sbin/sshd -e -D -f /Users/paulokstad/gitlab-development-kit/openssh/sshd_config
    ```
  - Run `gdk start sshd`
  - By default, SSHD will listen on `localhost:2222`
- Enable Prometheus for use with Gitaly
  - Enable Prometheus:
    ```Procfile
    # Procfile
    prometheus: exec docker run -p 9090:9090 -v /Users/paulokstad/gitlab-development-kit/prometheus/prometheus.yml:/etc/prometheus/prometheus.yml prom/prometheus:v2.8.1
    ```
    
  - Enable Praefect Prometheus listener
    ```toml
    # gitaly/praefect.config.toml
    prometheus_listen_addr = "127.0.0.1:10101"
    ```
    
  - Enable Gitaly Prometheus listeners
    ```toml
    # gitaly/gitaly-X.praefect.toml
    prometheus_listen_addr = "127.0.0.1:9236"
    ```
    - Note: use a unique port for each server
  - Update Prometheus to scrape Praefect & Gitalies **TODO**
