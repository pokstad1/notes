# Git Notes

## How-To
- Show log with one line per commit
  - `git config format.pretty oneline`
- Determine if commit is ancestor of another commit:
  - `git merge-base --is-ancestor $descendent $ancestor`
- Unstage  a file:
  - `git reset -- path1 path2`
    - Note: keeps working copy of file but unstages from index
- Split up commits:
  - Checkout the branch containing the big 'ol commit
  - `rebase -i master` (or to whichever commit/ref you want)
  - Mark all commits you want to split with `edit`
  - Reset to previous commit: `git reset HEAD^`
  - Carefully add changes for each new commit via `git add -p`
  - Continue adding commits until all changes are committed
  - Continue rebase operation: `git rebase --continue`
- Reset branch to head of remote
  - Useful for when you are following a branch that has rewritten history
  - Fetch all changes: `git fetch --all`
  - Reset to the new remote head: `git reset --hard origin/branch_name`

## Helpful Aliases

- force push with lease
  ```ini
  [alias]
  		pf = push --force-with-lease
  ```

## Education Resources
- Understanding Internals
    - [Git Internals](https://git-scm.com/book/en/v1/Git-Internals)
    - [Git Internals by Scott Chacon](https://github.com/pluralsight/git-internals-pdf)

## Interesting Projects
- Gitbase
    - [FOSDEM 2019 Git Merge talk on Gitbase](https://ftp.heanet.ie/mirrors/fosdem-video/2019/UD2.120/gitdatabase.mp4)

## Supporting Tools
- Signing commits
    - [Modern Alternatives to PGP](https://blog.gtank.cc/modern-alternatives-to-pgp/)
