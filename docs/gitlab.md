# GitLab Notes

## Tips and Tricks

- MRs
  - The query arg `?w=1` will omit whitespace from diffs in the GL webview
  
    - Note: the same can be done locally with `git diff -w`
  
  - This git alias will let you locally checkout with `git mr origin 1` (where `1` is the MR number):
    ```ini
    # ~/.gitconfig
    [alias]
    mr = !sh -c 'git fetch $1 merge-requests/$2/head:mr-$1-$2 && git checkout mr-$1-$2' -
    ```
    - [Source](https://docs.gitlab.com/ee/user/project/merge_requests/reviewing_and_managing_merge_requests.html#checkout-locally-by-adding-a-git-alias)
    
  - [One commit MRs](https://docs.gitlab.com/ee/user/project/push_options.html#push-options-for-merge-requests)
  
- Rails console

  - On Omnibus, access via `gitlab-rails c[onsole]`
  - To enable a Gitaly feature flag: `Feature.enable('gitaly_FEATURE_FLAG')`

## Competitors

- [SourceHut](https://sourcehut.org/)
    - Minimal take on integrated solution
    - Fast page loads due to lack of client side scripting